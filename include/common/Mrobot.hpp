#pragma once

#include <ros/ros.h>
#include <string>

template <  typename InputMsgT_supply_conveyor,
            typename OutputMsgT_supply_conveyor,
	        typename InputMsgT_camera,
	        typename OutputMsgT_camera,
	        typename InputMsgT_robot,
            typename OutputMsgT_robot,
	        typename InputMsgT_controller,
            typename OutputMsgT_controller,
	        typename InputMsgT_evacuation_conveyor,
            typename OutputMsgT_evacuation_conveyor,
            typename InputMsgT_assembly_station,
            typename OutputMsgT_assembly_station,
	        typename InputMsgT_robot_position,
            typename OutputMsgT_robot_position,
           typename InputMsgT_memoire,
            typename OutputMsgT_memoire>
class Mrobot {
public:
    Mrobot(ros::NodeHandle node, const std::string& topic)
        : node_(node) {


        output_publisher_supply_conveyor = node.advertise<InputMsgT_supply_conveyor>("supply_conveyor/input", 10);
        output_publisher_camera = node.advertise<InputMsgT_camera>("camera/input", 10);
        output_publisher_robot = node.advertise<InputMsgT_robot>("robot/input", 10);
        output_publisher_controller = node.advertise<InputMsgT_controller>("controller/input", 10);
        output_publisher_evacuation_conveyor = node.advertise<InputMsgT_evacuation_conveyor>("evacuation_conveyor/input", 10);
        output_publisher_assembly_station = node.advertise<InputMsgT_assembly_station>("assembly_station/input", 10);
        output_publisher_robot_position = node.advertise<InputMsgT_robot_position>("robot_position/input", 10);
         output_publisher_memoire = node.advertise<InputMsgT_memoire>("memoire/input", 10);

        input_subscriber_supply_conveyor = node.subscribe("supply_conveyor/output", 10, &Mrobot::input_supply_conveyor_Callback, this);
        input_subscriber_camera = node.subscribe("camera/output", 10, &Mrobot::input_camera_Callback, this);	    
        input_subscriber_robot = node.subscribe("robot/output", 10, &Mrobot::input_robot_Callback, this);	    
        input_subscriber_controller = node.subscribe("controller/output", 10, &Mrobot::input_controller_Callback, this);	    
        input_subscriber_evacuation_conveyor = node.subscribe("evacuation_conveyor/output", 10, &Mrobot::input_evacuation_conveyor_Callback, this);	    
        input_subscriber_assembly_station = node.subscribe("assembly_station/output", 10, &Mrobot::input_assembly_station_Callback, this);
        input_subscriber_robot_position = node.subscribe("robot_position/output", 10, &Mrobot::input_robot_position_Callback, this);
        input_subscriber_memoire = node.subscribe("memoire/output", 10, &Mrobot::input_memoire_Callback, this);

    }

    virtual void process() = 0;

protected:

    void sendOuputs_supply_conveyor(InputMsgT_supply_conveyor output_message) {
        output_publisher_supply_conveyor.publish(output_message);
    }
    void sendOuputs_camera(InputMsgT_camera output_message) {
    output_publisher_camera.publish(output_message);
    }
    void sendOuputs_robot(InputMsgT_robot output_message) {
    output_publisher_robot.publish(output_message);
    }
    void sendOuputs(InputMsgT_controller output_message) {
    output_publisher_controller.publish(output_message);
    }
    void sendOuputs_evacuation_conveyor(InputMsgT_evacuation_conveyor output_message) {
    output_publisher_evacuation_conveyor.publish(output_message);
    }
    //void sendOuputs_assembly_station(InputMsgT_assembly_station output_message) {
    //output_publisher_assembly_station.publish(output_message);}

    void sendOuputs_robot_position(InputMsgT_robot_position output_message) {
    output_publisher_robot_position.publish(output_message);
    }
    
    void sendOuputs_memoire(InputMsgT_memoire output_message) {
    output_publisher_memoire.publish(output_message);
    }


    OutputMsgT_supply_conveyor& getInputs_supply_conveyor() {
        return input_message_supply_conveyor;
    }
    OutputMsgT_camera& getInputs_camera() {
        return input_message_camera;
    }
    OutputMsgT_robot& getInputs_robot() {
        return input_message_robot;
    }
    OutputMsgT_controller& getInputs_controller() {
        return input_message_controller;
    }
    OutputMsgT_evacuation_conveyor& getInputs_evacuation_conveyor() {
        return input_message_evacuation_conveyor;
    }
    OutputMsgT_assembly_station& getInputs_assembly_station() {
        return input_message_assembly_station;
    }
    OutputMsgT_robot_position& getInputs_robot_position() {
        return input_message_robot_position;
    }
  OutputMsgT_memoire& getInputs_memoire() {
        return input_message_memoire;
   }
private:

    void input_supply_conveyor_Callback(const typename OutputMsgT_supply_conveyor::ConstPtr& message) {
        input_message_supply_conveyor = *message;
    }
    void input_camera_Callback(const typename OutputMsgT_camera::ConstPtr& message) {
        input_message_camera = *message;
    }
    void input_robot_Callback(const typename OutputMsgT_robot::ConstPtr& message) {
        input_message_robot = *message;
    }
    void input_controller_Callback(const typename OutputMsgT_controller::ConstPtr& message) {
        input_message_controller = *message;
    }
    void input_evacuation_conveyor_Callback(const typename OutputMsgT_evacuation_conveyor::ConstPtr& message) {
        input_message_evacuation_conveyor = *message;
    }
    void input_assembly_station_Callback(const typename OutputMsgT_assembly_station::ConstPtr& message) {
        input_message_assembly_station = *message;
    }
    void input_robot_position_Callback(const typename OutputMsgT_robot_position::ConstPtr& message) {
        input_message_robot_position = *message;
    }
  void input_memoire_Callback(const typename OutputMsgT_memoire::ConstPtr& message) {
        input_message_memoire = *message;
    }
 
    ros::NodeHandle node_;

    ros::Publisher output_publisher_supply_conveyor;
    ros::Subscriber input_subscriber_supply_conveyor;

    ros::Publisher output_publisher_camera;
    ros::Subscriber input_subscriber_camera;

    ros::Publisher output_publisher_robot;
    ros::Subscriber input_subscriber_robot;

    ros::Publisher output_publisher_controller;
    ros::Subscriber input_subscriber_controller;

    ros::Publisher output_publisher_evacuation_conveyor;
    ros::Subscriber input_subscriber_evacuation_conveyor;

    ros::Publisher output_publisher_assembly_station;
    ros::Subscriber input_subscriber_assembly_station;

    ros::Publisher output_publisher_robot_position;
    ros::Subscriber input_subscriber_robot_position;

     ros::Publisher output_publisher_memoire;
    ros::Subscriber input_subscriber_memoire;

    OutputMsgT_supply_conveyor input_message_supply_conveyor;
    OutputMsgT_camera input_message_camera;
    OutputMsgT_robot input_message_robot;
    OutputMsgT_controller input_message_controller;
    OutputMsgT_evacuation_conveyor input_message_evacuation_conveyor;
    OutputMsgT_assembly_station input_message_assembly_station;
    OutputMsgT_robot_position input_message_robot_position;
    OutputMsgT_memoire input_message_memoire;


};
