#include <ros/ros.h>
#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <common/machine_controller.hpp>


class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_output> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::START) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::START:
            if (inputs.part1_ASS && inputs.part2_ASS && inputs.part3_ASS) {
		inputs.part1_ASS = false;
		inputs.part2_ASS = false;
		inputs.part3_ASS = false;
        outputs.ascheck = true;
        sendOuputs(outputs);
                ROS_INFO("[AssemblyStation] ASS_VALID");
                state_ = State::ASS_CHECK;
            }
            break;

        case State::ASS_CHECK:
            if (getState().valid) {
                outputs.checkvalid= true;
                sendOuputs(outputs);
                ROS_INFO("[AssemblyStation] VerificationValider");
                state_ = State::VALID;
            }
            break;

	     case State::VALID:
            if (getState().evacuated) {
                outputs.evacuated= true;
		sendOuputs(outputs);
                ROS_INFO("[AssemblyStation] EvacuationDesTroisPieces");
                state_ = State::START;
            }
            break;

        }

        sendCommands(commands);
    }

private:
    enum class State { START, VALID, ASS_CHECK };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation ass_station(node);

    while (ros::ok()) {
        ass_station.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}



