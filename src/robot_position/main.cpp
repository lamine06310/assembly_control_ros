#include <ros/ros.h>

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/camera_output.h>
#include <assembly_control_ros/camera_input.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/controller_input.h>
#include <assembly_control_ros/controller_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <assembly_control_ros/robot_position_output.h>
#include <assembly_control_ros/robot_position_input.h>

#include <assembly_control_ros/memoire_input.h>
#include <assembly_control_ros/memoire_output.h>



#include <common/Mrobot.hpp>

class Robot_Position
	: public Mrobot< assembly_control_ros::supply_conveyor_input,
         assembly_control_ros::supply_conveyor_output,
	assembly_control_ros::camera_input,
       assembly_control_ros::camera_output,
	assembly_control_ros::robot_input,
	assembly_control_ros::robot_output,
        assembly_control_ros::controller_input,
	assembly_control_ros::controller_output,
        assembly_control_ros::evacuation_conveyor_input,
	assembly_control_ros::evacuation_conveyor_output,
        assembly_control_ros::assembly_station_input,
        assembly_control_ros::assembly_station_output,
	assembly_control_ros::robot_position_input,
        assembly_control_ros::robot_position_output,
	assembly_control_ros::memoire_input,
	assembly_control_ros::memoire_output > {
public:
	Robot_Position(ros::NodeHandle node)
		:Mrobot(node, "Robot_Position"), state_(State::robot_position_begining) {
	}

	virtual void process() override {
          
                	assembly_control_ros::robot_position_output outputs;
		        assembly_control_ros::robot_position_input inputs;
                        assembly_control_ros::supply_conveyor_input scoutputs;
                         assembly_control_ros::camera_input camoutputs;
                          assembly_control_ros::robot_input roboutputs;
   assembly_control_ros::robot_input roboinputs;
		        //assembly_control_ros::assembly_station_input roboutputs;
		        //assembly_control_ros::evacuation_conveyor_input ecoutputs;
                         assembly_control_ros::memoire_input memoutputs;
		     
		       
		     

		            auto& scinputs = getInputs_supply_conveyor();
                            auto& caminputs = getInputs_camera();
                            auto& robinputs = getInputs_robot();
                            auto& continputs = getInputs_controller();
                            auto& ecinputs = getInputs_evacuation_conveyor();
                            auto& asinputs = getInputs_assembly_station();
                            auto& meminputs = getInputs_memoire();

       
           switch (state_) {

        case State::Assemblage:
           
            if (robinputs.GO_RIGHT) {
                robinputs.GO_RIGHT = false;
                ROS_INFO("[RobotPosition] right ");
                state_ = State::ROB_RIGHT_SC;
            }
            break;

        case State::ROB_RIGHT_SC:
            if (getState().ROB_AT_SC) {
                outputs.SCC = true;
                sendOuputs(outputs);
                ROS_INFO("[RobotPosition] Robot_AT_SC");
                state_ = State::SCSC;
				//le robot est a SC
            }

            break;

        case State::SCSC:
            if (inputs.GO_RIGHT) {
                inputs.GO_RIGHT = false;
                ROS_INFO("[RobotPosition] Rob_SC_EC");
                state_ = State::ROB_RIGHT_SC.EC;
            }
			else if (inputs.GO_LEFT)
            {
			
			inputs.GO_LEFT = false;
                ROS_INFO("[RobotPosition] Rob_SC_ASS");
                state_ = State::ROB_LEFT_SC.ASS;
				
			}

            break;
        case State::ROB_LEFT_SC.EC; if (getState().ROB_AT_EC) {
            outputs.ECC = true;
            sendOuputs(outputs);
            ROS_INFO("[RobotPosition] Robot_AT_EC");
            state_ = State::EC;
            //le robot est a EC
        }
            break;
            case State::EC;
            if (inputs.GO_LEFT) {
                inputs.GO_LEFT = false; ROS_INFO("[RobotPosition] Robot_EC_SC");
                state_ = State::ROB_LEFT_EC.SC;
            }
            //le robot se deplace de EC vers SC
            break;
            case State::ROB_LEFT_EC.SC:
            if (getState().ROB_AT_SC_1){
                outputs.SCC = true;
                sendOuputs(outputs);
                ROS_INFO("[RobotPosition] Robot_AT_SC");
                state_ = State::SCSC;
            }
             //le robot est revenu au SC

				break;
                case State::ROB_LEFT_SC.ASS; 
                if (getState().ROB_AT_AS){
                    outputs.ASS = true;
                    sendOuputs(outputs);
                    ROS_INFO("[RobotPosition] Robot_AT_SC");
                    state_ = State::ASSEMBLAGE;
			//LE ROBOT EST REVENU AU POSTE D'ASSEMBLAGE

            }
           }
            
            
    }

private:
                    enum class State {
                        ASSEMBLAGE, SCSC, ROB_LEFT_EC_SC, EC, ROB_LEFT_SC_ASS, ROB_RIGHT_SC_EC, SC_SC, ROB_RIGHT_SC};

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "Robot_Position");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot_Position Robot_Position(node);

    while (ros::ok()) {
        Robot_Position.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
        
