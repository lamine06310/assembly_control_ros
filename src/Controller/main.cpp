#include <ros/ros.h>

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/camera_output.h>
#include <assembly_control_ros/camera_input.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/controller_input.h>
#include <assembly_control_ros/controller_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <assembly_control_ros/robot_position_output.h>
#include <assembly_control_ros/robot_position_input.h>

#include <assembly_control_ros/memoire_input.h>
#include <assembly_control_ros/memoire_output.h>



#include <common/Mrobot.hpp>

class Controller
	: public Mrobot< assembly_control_ros::supply_conveyor_input,
         assembly_control_ros::supply_conveyor_output,
	assembly_control_ros::camera_input,
       assembly_control_ros::camera_output,
	assembly_control_ros::robot_input,
	assembly_control_ros::robot_output,
        assembly_control_ros::controller_input,
	assembly_control_ros::controller_output,
        assembly_control_ros::evacuation_conveyor_input,
	assembly_control_ros::evacuation_conveyor_output,
        assembly_control_ros::assembly_station_input,
        assembly_control_ros::assembly_station_output,
	assembly_control_ros::robot_position_input,
        assembly_control_ros::robot_position_output,
	assembly_control_ros::memoir_input,
	assembly_control_ros::memoir_output > {
public:
	Controller(ros::NodeHandle node)
		:Mrobot(node, "controller"), state_(State::START) {
	}

	virtual void process() override {
          
                	assembly_control_ros::controller_output outputs;
		        assembly_control_ros::controller_input inputs;
                        assembly_control_ros::supply_conveyor_input scoutputs;
                         assembly_control_ros::camera_input camoutputs;
                          assembly_control_ros::robot_input roboutputs;
		        assembly_control_ros::assembly_station_input roboutputs;
		        assembly_control_ros::evacuation_conveyor_input ecoutputs;
                             assembly_control_ros::memoire_input memoutputs;
		     
		       
		     

		            auto& scinputs = getInputs_supply_conveyor();
                            auto& caminputs = getInputs_camera();
                            auto& robinputs = getInputs_robot();
                            auto& continputs = getInputs_controller();
                            auto& ecinputs = getInputs_evacuation_conveyor();
                            auto& asinputs = getInputs_assembly_station();
                            auto& meminputs = getInputs_memoire();

		switch (state_) {

		case State::START:
                             ros::Duration(5.0).sleep();
			outputsROB.GO_RIGHT = true;
			sendOuputROB(outputsROB);
			ROS_INFO("[controller] deplacer vers SC");
			state_ = State::ROB_RIGHT;
			break;

		case State::ROB_RIGHT:
			while (not input.SC)
				ros::Duration(3.0).sleep();//le temps d'arriver a SC
			inputsSC.SC = false;
			outputs.AT_SC = true;
			sendOuputROB(outputsROB);
			ROS_INFO("[controller] arriver vers SC");
			state_ = State::AT_SC;
			break;

		case State::AT_SC:
			while (not input.CAM_PROCESSING)
				ros::Duration(3.0).sleep();//le temps de la reconnaissance de la piece
			input.CAM_PROCESSING = false;
			outputs.GRASP = true;
			sendOuput(GRASP);
			ROS_INFO("[controller] arreter a SC");
			state_ = State::ROB_GRASP;

			break;

		case State::ROB_GRASP:
			while (not input.GRASPED)
				ros::Duration(3.0).sleep();//le temps de prendre e la piece
			input.GRASPED = false;
			ROS_INFO("[controller] la piece est prise");
			state_ = State::PARTX_GRASPED;

			break;

		case State::PARTX_GRASPED:
			if (ROB_DONE_ASS) // si la piece est juste
			{
				inputs.ROB_DONE_ASS = false;
				outputs.GO_LEFT = true;
				sendOutput(outputROB);
				outputs.START_SC true;
				sendOutput(outputSC);
				ros::Duration(3.0).sleep();//le temps de prendre e la piece
				ROS_INFO("[controller] la piece est accepter");
				state_ = State::ROB_LEFT;
			}
			else if (ROB_DONE_EC) // si la piece est rejeter
			{
				inputs.ROB_DONE_EC = false;
				outputs.STOP_EC = true;
				sendOutput(outputEC);
				outputs.GO_RIGHT = true;
				sendOutput(outputROB);
				outputs.START_SC = true;
				sendOutput(outputSC);
				ros::Duration(3.0).sleep();//le temps d'arreter le EC
				ROS_INFO("[controller] la piece est REJETTER");
				state_ = State::ROB_RIGHT;
			}

			break;

		case State::ROB_RIGHT:
			while (not input.ROB_EC)
				ros::Duration(3.0).sleep();//le temps que le robot arrive au poste EC
			input.ROB_RIGHT = false;
			outputs.STOP_RIGHT = true;
			sendOutput(outputROB);
			ROS_INFO("[controller] le robot a EC");
			state_ = State::ROB_ec;

			break;
		case State::ROB_ec:
			while (not input.STOP_ec)
				ros::Duration(3.0).sleep();
			input.STOP_ec = false;
			outputs.RELEASE = true;
			sendOutput(outputROB);
			ROS_INFO("[controller] release partx");
			state_ = State::EC_STOPP;

			break;
		case State::EC_STOPP:
			while (not input.RELEASE_PARTX)
				ros::Duration(3.0).sleep();
			input.RELEASE_PARTX = false;
			outputs.START_EC = true;
			sendOutput(outputEC);
			outputs.PARTX_RELEASED = true;
			sendOutput(outputMEM);
			ROS_INFO("[controller] robot deplace a gauche");
			state_ = State::ROB_LEFT_SC;

			break;
		case State::ROB_LEFT:
			while (not input.SC)
				ros::Duration(3.0).sleep();
			input.SC = false;
			ROS_INFO("[controller] robot arreter a SC");
			state_ = State::AT_SC;

			//dans le cas ou la piece est juste 

			break;
		case State::ROB_LEFT:
			if (inputsROB.ROB_ASS)
			{
				inputs.ROB_ASS = false;

				ros::Duration(3.0).sleep();
				ROS_INFO("[controller] teste");
				state_ = State::ROB_AT_AS;
			}

			break;
		case State::ROB_AT_AS:
			if (inputROB.PART1V)
			{
				inputs.PART1_V = false;
				outputROB.ROB_ASS_PART1 = true;
				sendOutput(outputROB);
				ros::Duration(3.0).sleep();
				ROS_INFO("[controller] PART1");
				state_ = State::rob_ass_part1;
			}
			else if (inputROB.PART2_V)
			{
				inputs.PART2_V = false;
				outputROB.ROB_ASS_PART2 = true;
				sendOutput(outputROB);
				ros::Duration(3.0).sleep();
				ROS_INFO("[controller] PART2");
				state_ = State::rob_ass_part2;
			}
			else if (inputROB.PART3_V)
			{
				inputs.PART3_V = false;
				outputROB.ROB_ASS_PART3 = true;
				sendOutput(outputROB);
				ros::Duration(3.0).sleep();
				ROS_INFO("[controller] PART2");
				state_ = State::rob_ass_part3;
			}


			break;
		case State::rob_ass_part1:
			while (not input.PART1 - ASS)
				ros::Duration(3.0).sleep();
			input.PARTA1_ASS = false;
			outputCAM.PART1_PART1 = true;
			sendOutput(outputCAM);
			ROS_INFO("[controller] begin");
			state_ = State::START;

			break;
		case State::rob_ass_part2:
			while (not input.PART2 - ASS)
				ros::Duration(3.0).sleep();
			input.PARTA1_ASS = false;
			outputCAM.PART1_PART2 = true;
			sendOutput(outputCAM);
			ROS_INFO("[controller] begin");
			state_ = State::START;

			break;
		case State::rob_ass_part3:
			while (not input.PART3 - ASS)
				ros::Duration(3.0).sleep();
			input.PARTA1_ASS = false;
			outputCAM.PART1_PART3 = true;
			sendOutput(outputCAM);
			ROS_INFO("[controller] begin");
			state_ = State::START;

		}
	}
	private:

    bool want_Part1 = true;
    bool want_Part2 = true;
    bool want_Part3 = true;
    int Place = 0;

    enum class State {START, Go_right, Suply_convyor1, Suply_convyor2, Grasp, Memory_Decision, Ec_Valid, GO_Right2, arrived, Left1, Left2, Ass_Valid, part1_ready, Analysis_part1, part2_ready, Analysis_part2, part3_ready, Analysis_part3};

    State state_;

};
int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controleur");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controller controller(node);

    while (ros::ok()) {
        controller.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}




			