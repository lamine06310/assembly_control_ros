#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>

class Robot
    : public MachineController<assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Idle) {
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::Idle:

		
		if (inputs.deplacer_a_droite) {
		inputs.deplacer_a_droite = false;
		ROS_INFO("[robot] RIGHT");
		state_ = State::RIGHT;

		}
		else if (inputs.deplacer_a_gauche) {
			inputs.deplacer_a_gauche = false;
			ROS_INFO("[robot] LEFT");
			state_ = State::LEFT;
		}
		else if (inputs.grasp_PARTX) {
			inputs.grasp_PARTX = false;
			ROS_INFO("[robot] GRASP");
			state_ = State::GRASP;
		}
		else if (inputs.release_PARTX) {
			inputs.release_PARTX = false;
			ROS_INFO("[robot] RELEASE");
			state_ = State::RELEASE;
		}
		else if (inputs.faire_PART1) {
			inputs.faire_PART1 = false;
			ROS_INFO("[robot]ASS_PART1");
			state_ = State::ASS_PART1;
		}
		else if (inputs.faire_PART2) {
			inputs.faire_PART2 = false;
			ROS_INFO("[robot] ASS_PART2");
			state_ = State::ASS_PART2;
		}
		else if (inputs.faire_PART3) {
			inputs.faire_PART3 = false;
			ROS_INFO("[robot] ASS_PART3");
			state_ = State::ASS_PART3;
		}


		    break;

        case State::RIGHT:
			commands.move_right = true;
			if (getState().at_supply_conveyor || getState().at_assembly_station) {
				outputs.ROB_RIGHT = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] Idle");
				state_ = State::Idle;
			}
			break;

		case State::LEFT:
			commands.move_left = true;
			if (getState().at_supply_conveyor || getState().at_assembly_station) {
				outputs.ROB_LEFT = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] Idle");
				state_ = State::Idle;
			}
	        break;

	    case State::GRASP:
			commands.grasp = true;
			if (getState().part_grasped) {
				outputs.ROB_GRASP = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] idle");
				state_ = State::Idle;
			}
			break;

		case State::RELEASE:
			commands.release= true;
			if (getState().part_released) {
				outputs.PARTX_RELEASEd = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] idle");
				state_ = State::Idle;
			}
			break;

		case State::ASS_PART1:
			commands.assemble_part1= true;
			if (getState().part1_assembled) {
				outputs.fin_PART1 = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] idle");
				state_ = State::Idle;
			}
			break;

		case State::ASS_PART2:
			commands.assemble_part3 = true;
			if (getState().part2_assembled) {
				outputs.fin_PART2 = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] idle");
				state_ = State::Idle;
			}
			break;

		case State::ASS_PART3:
			commands.assemble_part3 = true;
			if (getState().part3_assembled) {
				outputs.fin_PART3 = true;
				sendOuputs(outputs);
				ROS_INFO("[robot] idle");
				state_ = State::Idle;
				
			}
			break;


        }

        sendCommands(commands);
    }

private:
    enum class State { Idle, LEFT, RIGHT, GRASP, RELEASE, ASS_PART1, ASS_PART2, ASS_PART3 };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
  