#include <ros/ros.h>

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/camera_output.h>
#include <assembly_control_ros/camera_input.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/controller_input.h>
#include <assembly_control_ros/controller_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <assembly_control_ros/robot_position_output.h>
#include <assembly_control_ros/robot_position_input.h>

#include <assembly_control_ros/memoire_input.h>
#include <assembly_control_ros/memoire_output.h>



#include <common/Mrobot.hpp>

class Memoire
	: public Mrobot< assembly_control_ros::supply_conveyor_input,
         assembly_control_ros::supply_conveyor_output,
	assembly_control_ros::camera_input,
       assembly_control_ros::camera_output,
	assembly_control_ros::robot_input,
	assembly_control_ros::robot_output,
        assembly_control_ros::controller_input,
	assembly_control_ros::controller_output,
        assembly_control_ros::evacuation_conveyor_input,
	assembly_control_ros::evacuation_conveyor_output,
        assembly_control_ros::assembly_station_input,
        assembly_control_ros::assembly_station_output,
	assembly_control_ros::robot_position_input,
        assembly_control_ros::robot_position_output,
	assembly_control_ros::memoire_input,
	assembly_control_ros::memoire_output > {
public:
	Memoire(ros::NodeHandle node)
		:Mrobot(node, "Memoire"), state_(State::start) {
	}

	virtual void process() override {
          
                	assembly_control_ros::robot_position_output outputs;
		        assembly_control_ros::robot_position_input inputs;
                        assembly_control_ros::supply_conveyor_input scoutputs;
                         assembly_control_ros::camera_input camoutputs;
                          assembly_control_ros::robot_input roboutputs;
   assembly_control_ros::robot_input roboinputs;
		        assembly_control_ros::assembly_station_input roboutputs;
		        assembly_control_ros::evacuation_conveyor_input ecoutputs;
                         assembly_control_ros::memoire_input memoutputs;
		     
		       
		     

		            auto& scinputs = getInputs_supply_conveyor();
                            auto& caminputs = getInputs_camera();
                            auto& robinputs = getInputs_robot();
                            auto& continputs = getInputs_controller();
                            auto& ecinputs = getInputs_evacuation_conveyor();
                            auto& asinputs = getInputs_assembly_station();
                            auto& meminputs = getInputs_memoire();
 switch (state_) {

        case State::start:

   START_on = true;
		PART1_wait = true;
		PART2_wait = true;
		PART3_wait = true;
		while
		{
			if (START - ON = true && PART1_wait = true && input.CAM_PART1 = true)
			{
				START - ON = false;
				PART1_wait = false;
				input.CAM_PART1 = false;
				ROB_GRASP_P1 = true;
				ROS_INFO("[memoire] la part1 est prise");

			}
			else if (START - ON = true && PART2_wait = true && input.CAM_PART2 = true)
			{
				START - ON = false;
				PART1_wait = false;
				input.CAM_PART2 = false;
				ROB_GRASP_P2 = true;
				ROS_INFO("[memoire] la part2 est prise");
			}
			else if (START - ON = true && PART3_wait = true && input.CAM_PART3 = true)
			{
				START - ON = false;
				PART1_wait = false;
				input.CAM_PART1 = false;
				ROB_GRASP_P3 = true;
				ROS_INFO("[memoire] la part3 est prise");
			}
			//////////////////////////////////////////////////////////
			if (START - ON = true && operation part1 = true && input.CAM_PART1 = true)
			{
				START - ON = false;
				operation part1 = false;
				input.CAM_PART1 = false;
				PART1 - INVALID = true;
				ROS_INFO("[memoire] la part1 est refusé");
			}
			else if (START - ON = true && operation part2 = true && input.CAM_PART2 = true)
			{
				START - ON = false;
				operation part2 = false;
				input.CAM_PART2 = false;
				PART2 - INVALID = true;
				ROS_INFO("[memoire] la part2 est refusé");
			}
			else if (START - ON = true && operation part3 = true && input.CAM_PART3 = true)
			{
				START - ON = false;
				operation part3 = false;
				input.CAM_PART3 = false;
				PART3 - INVALID = true;
				ROS_INFO("[memoire] la part3 est refusé");
			}
			/////////////////////////////////////////////////////////////////

			if (ROB - GRASP - P1 = true;)

			{
				ROB - GRASP - P1 = false;
				//dans ce cas opération1 est valide
				START - ON = true

					output.PARTX_VALID = true
					sendOuput(outputs);
				output.ROB_ASS_PART1 = true;
				sendOuput(outputs);
				ROS_INFO("[memoire] piece juste");
				operation - part1 = true;
			}
			else if (ROB - GRASP - P2 = true;)

			{
				ROB - GRASP - P2 = false;
				//dans ce cas opération1 est valide
				START - ON = true

					output.PARTX_VALID = true
					sendOuput(outputs);
				output.ROB_ASS_PART2 = true;
				sendOuput(outputs);
				ROS_INFO("[memoire] piece juste");
				operation - part2 = true;
			}

			else if (ROB - GRASP - P3 = true;)

			{
				ROB - GRASP - P3 = false;
				//dans ce cas opération1 est valide
				START - ON = true

					output.PARTX_VALID = true
					sendOuput(outputs);
				output.ROB_ASS_PART3 = true;
				sendOuput(outputs);
				ROS_INFO("[memoire] piece juste");
				operation - part3 = true;
			}
			/////////////////////////////////////////////////
			if (ROB_ASS_P1 = true)
			{
				if (input.CAM_PART1 = true && START = true)
				{
					input.CAM_PART1 = false;
					START = false;
					input.CAM_PART1 = false;
					ROS_INFO("[memoire] attente du signale de la cam");
					PART1_INVALID = true;

				}
				else if (ROB_ASS_P2 = true && ROB_ASS_P3 = true)
				{
					ROB_ASS_P2 = false;
					ROB_ASS_P3 = false;
					ROB_ASS_P1 = false;

					PART1 = true;
					PART2 = true;
					PART3 = true;
				}

				if (ROB_ASS_P2 = true)
				{
					if (input.CAM_PART2 = true && START = true)
					{
						input.CAM_PART2 = false;
						START = false;
						input.CAM_PART2 = false;
						ROS_INFO("[memoire] attente du signale de la cam");
						PART2_INVALID = true;

					}
					else if (ROB_ASS_P1 = true && ROB_ASS_P3 = true)
					{
						ROB_ASS_P1 = false;
						ROB_ASS_P2 = false;
						ROB_ASS_P3 = false;

						PART1 = true;
						PART2 = true;
						PART3 = true;
					}


					if (ROB_ASS_P3 = true)
					{
						if (input.CAM_PART3 = true && START = true)
						{
							input.CAM_PART3 = false;
							START = false;
							input.CAM_PART3 = false;
							ROS_INFO("[memoire] attente du signale de la cam");
							PART1_INVALID = true;

						}
						else if (ROB_ASS_P1 = true && ROB_ASS_P2 = true)
						{
							ROB_ASS_P1 = false;
							ROB_ASS_P2 = false;
							ROB_ASS_P3 = false;

							PART1 = true;
							PART2 = true;
							PART3 = true;
						}
					}
					////////////////////////////////////////////////////////////
				}
				if (PART1_INVALID = true)
				{
					PART1_INVALID = false;

					PARTX_INVALID = true;
					output.PART_invalid = true;
					sendOuput(outputs);
					ROS_INFO("[memoire] piece 1 rejeter");
					ROB_ASS_P1 = true;

				}
				else if (PART2_INVALID = true)
				{
					PART2_INVALID = false;

					PARTX_INVALID = true;
					output.PART_invalid = true;
					sendOuput(outputs);
					ROS_INFO("[memoire] piece 2 rejeter");
					ROB_ASS_P2 = true;

				}
				else if (PART3_INVALID = true)
				{
					PART1_INVALID = false;

					PARTX_INVALID = true;
					output.PART_invalid = true;
					sendOuput(outputs);
					ROS_INFO("[memoire] piece3 rejeter");
					ROB_ASS_P3 = true;

				}

				if (PARTX_INVALID = true;)
				{
					while (not input.ROB_RELEASE = true)
					{
						ros::Duration(3.0).sleep();
					}
					ROB_RELEASE = false;
					PARTX_INVALID = false;
					START - ON = true;

            }
        }


private:
                    enum class State {
                        ASSEMBLAGE, SCSC, ROB_LEFT_EC_SC, EC, ROB_LEFT_SC_ASS, ROB_RIGHT_SC_EC, SC_SC, ROB_RIGHT_SC};

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "memoire");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Memoire memoire(node);

    while (ros::ok()) {
        Memoire.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}      

   



