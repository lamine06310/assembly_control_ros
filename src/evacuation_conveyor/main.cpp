#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <common/machine_controller.hpp>

class EvacuationConveyor
    : public MachineController<assembly_control_ros::evacuation_conveyor_state,
                               assembly_control_ros::evacuation_conveyor_input,
                               assembly_control_ros::evacuation_conveyor_command,
                               assembly_control_ros::evacuation_conveyor_output> {
public:
	EvacuationConveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::EC_ON) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::EC_ON:
            commands.on = true;
            if (inputs.EC_OFF) {
                inputs.EC_OFF = false;
                ROS_INFO("[EvacuationConveyor] Off");
                state_ = State::Arretencours;
			}
			break;

		case State::Arretencours:
			if (getState().stopped) {
				outputs.stopped = true;
				sendOuputs(outputs);
				ROS_INFO("[EvacuationConveyor] Off");
				state_ = State::EC_STOP;
			}
            break;

        case State::EC_STOP:
            if (inputs.RESTART_EC) {
                inputs.RESTART_EC = false;
                ROS_INFO("[EvacuationConveyor] On");
                state_ = State::EC_ON;

            }
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { EC_ON, EC_STOP, Arretencours };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

	EvacuationConveyor conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


